/*
 * Attaches the image uploader to the input field
 */
jQuery(document).ready(function($){

	  var file_frame;

	  $(document).on('click', '#gallery-metabox button.gallery-add', function(e) {

	    e.preventDefault();

	    if (file_frame) file_frame.close();

	    file_frame = wp.media.frames.file_frame = wp.media({
	      title: $(this).data('uploader-title'),
	      button: {
	        text: $(this).data('uploader-button-text'),
	      },
	      multiple: true
	    });

	    file_frame.on('select', function() {
	      var listIndex = $('#gallery-metabox-list li').index($('#gallery-metabox-list li:last')),
	          selection = file_frame.state().get('selection');

	      selection.map(function(attachment, i) {
	        attachment = attachment.toJSON(),
	        index      = listIndex + (i + 1);

	        $('#gallery-metabox-list').append('<li><input type="hidden" name="meta-service-gallery-id[' + index + '][id]" value="' + attachment.id + '"><img class="image-preview" src="' + attachment.sizes.thumbnail.url + '"><textarea name="meta-service-gallery-id[' + index + '][caption]" value="' + attachment.caption + '" /></textarea><button class="change-image button button-small" type="button" data-uploader-title="Change image" data-uploader-button-text="Change image">Change image</button><br><small><button class="remove-image button button-small" type="button">Remove image</button></small></li>');
	        // console.log(attachment);
	      });
	    });

	    makeSortable();
	    
	    file_frame.open();

	  });

	  $(document).on('click', '#gallery-metabox button.change-image', function(e) {

	    e.preventDefault();

	    var that = $(this);

	    if (file_frame) file_frame.close();

	    file_frame = wp.media.frames.file_frame = wp.media({
	      title: $(this).data('uploader-title'),
	      button: {
	        text: $(this).data('uploader-button-text'),
	      },
	      multiple: false
	    });

	    file_frame.on( 'select', function() {
	      attachment = file_frame.state().get('selection').first().toJSON();

	      that.parent().find('input:hidden').attr('value', attachment.id);
	      that.parent().find('img.image-preview').attr('src', attachment.sizes.thumbnail.url);
	    });

	    file_frame.open();

	  });

	  function resetIndex() {
	    $('#gallery-metabox-list li').each(function(i) {
	      $(this).find('input:hidden').attr('name', 'meta-service-gallery-id[' + i + '][id]');
	      $(this).find('input:text').attr('name', 'meta-service-gallery-id[' + i + '][caption]');
	    });
	  }

	  function makeSortable() {
	    $('#gallery-metabox-list').sortable({
	      opacity: 0.6,
	      stop: function() {
	        resetIndex();
	      }
	    });
	  }

	  $(document).on('click', '#gallery-metabox button.remove-image', function(e) {
	    e.preventDefault();

	    $(this).parents('li').animate({ opacity: 0 }, 200, function() {
	      $(this).remove();
	      resetIndex();
	    });
	  });

	  makeSortable();

});