<?php
/*
Template Name: Gallery Page
*/

get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

query_posts(
	array(
		'post_type'	=> 'services',
		'orderby'		=> 'date',
		'order'			=> 'DESC'
	)
);

?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container gallery-container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
				<div class="entry-content et_pb_gutters1 gallery-list-page">

	<?php endif; ?>

				<?php if( have_posts() ) : while ( have_posts() ) : the_post(); ?>

					<?php 
						$slug = get_post_field( 'post_name', get_post() );
						$images = get_post_meta( get_the_ID(), 'meta-service-gallery-id', true );
					 ?>
					
					<?php if (!empty($images)): ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="et_pb_column et_pb_column_1_2 et_pb_column_0">
								<a href="http://kjthomeimprovements.co.uk/services/<?php echo $slug; ?>/gallery/">
									<?php the_post_thumbnail( 'full' ); ?>
									<h1 class="main_title"><?php the_title(); ?></h1>
								</a>
							</div>
						</article> <!-- .et_pb_post -->
					<?php endif; ?>

				<?php endwhile; endif; ?>
				<div class="et_pb_column et_pb_column_1_2 et_pb_column_1">
					<span><a href="http://kjthomeimprovements.co.uk/contact-us/">Contact us</a> &<br>see what we<br>can do for you!<br><i class="fa fa-envelope"></i></span>
				</div>


				</div> <!-- #left-area -->

			</div> <!-- #content-area -->
		</div> <!-- .container -->

	</div> <!-- #main-content -->
</div>

<?php get_footer(); ?>