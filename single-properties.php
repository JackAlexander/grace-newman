<?php get_header(); ?>

<div id="main-content">

	<?php while ( have_posts() ) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php
			$slug = get_post_field( 'post_name', get_post() );
			$property_container = get_post_meta( get_the_ID(), 'meta-slider-container', true );
			$property_thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
			$property_status = get_post_meta( get_the_ID(), 'meta-status', true );
			$property_type = get_post_meta( get_the_ID(), 'meta-type', true );
			$property_location = get_post_meta( get_the_ID(), 'meta-location', true );
			$property_floor_plan = get_post_meta( get_the_ID(), 'meta-floor-plan', true );
			$property_features = get_post_meta( get_the_ID(), 'meta-property-features', true );
			$property_floor_plan_text_list = explode("\n", $property_features);
			$apartment_features = get_post_meta( get_the_ID(), 'meta-apartment-features', true );
			$apartment_floor_plan_text_list = explode("\n", $apartment_features);
			$property_bedrooms = get_post_meta( get_the_ID(), 'facility-bedrooms', true );
			$property_people = get_post_meta( get_the_ID(), 'facility-people', true );
			$property_bathrooms = get_post_meta( get_the_ID(), 'facility-bathrooms', true );


			$images = get_post_meta($post->ID, 'vdw_gallery_id', true);


			$property_images = array();
			$property_images_path = $_SERVER['DOCUMENT_ROOT'] . '/img/villas/' . $slug;
			$property_images_url = get_home_url() . '/img/villas/' . $slug . '/';
			$property_images_scan = array_slice(glob($property_images_path . '/*.{jpg,png}', GLOB_BRACE | GLOB_NOSORT),0,8);
			foreach ($property_images_scan as $property_images_scan_result){
				$property_images[] = $property_images_url . basename($property_images_scan_result);
			}
		?>
			<div class="entry-content">

				<?php if (!empty($images)): ?>
					<div class="property-slider-container">
						<div class="property-slider <?php if(!$property_container) echo "et_pb_row"; ?> owl-carousel owl-theme" data-slider-id="1">
							<?php foreach ($images as $image): ?>
								<div><?php echo wp_get_attachment_image($image,'large') ?></div>
							<?php endforeach; ?>
						</div>
						<div class="property-thumbs" data-slider-id="1">
							<?php foreach ($images as $image): ?>
								<button class="property-thumb-item"><?php echo wp_get_attachment_link($image,'large') ?></button>
							<?php endforeach; ?>
						</div>
					</div>
				<?php else: ?>
					<div class="property-slider-container">
						<div class="property-slider owl-carousel owl-theme <?php if(!$property_container) echo "et_pb_row"; ?>">
							<div>
								<?php the_post_thumbnail(); ?>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<div class="et_pb_section et_pb_section_1 et_section_regular property-section-main">

					<div class="et_pb_row et_pb_row_2">
						<div class="et_pb_column et_pb_column_1_3 et_pb_column_0">

							<div class="prop-info">
								<h1>Property Detail</h1>
								<?php if (!empty($property_type)): ?>
									<?php
									$output = '<span class="prop-info-col-1">Type</span>';
									$output .= '<span class="prop-info-col-2">';
									$output .= $property_type;
									$output .= "</span>";
									echo $output;
									?>
								<?php endif ?>
								<?php if (!empty($property_status)): ?>
									<?php
									$output = '<span class="prop-info-col-1">Status</span>';
									$output .= '<span class="prop-info-col-2">';
									$output .= $property_status;
									$output .= "</span>";
									echo $output;
									?>
								<?php endif ?>
								<?php if (!empty($property_location)): ?>
									<?php
									$output = '<span class="prop-info-col-1">Location</span>';
									$output .= '<span class="prop-info-col-2">';
									$output .= $property_location;
									$output .= "</span>";
									echo $output;
									?>
								<?php endif ?>
								<?php if (!empty($property_bedrooms)): ?>
									<?php
									$output = '<span class="prop-info-col-1">Bedrooms</span>';
									$output .= '<span class="prop-info-col-2">';
									$output .= $property_bedrooms;
									$output .= "</span>";
									echo $output;
									?>
								<?php endif ?>
								<?php if (!empty($property_bathrooms)): ?>
									<?php
									$output = '<span class="prop-info-col-1">Bathrooms</span>';
									$output .= '<span class="prop-info-col-2">';
									$output .= $property_bathrooms;
									$output .= "</span>";
									echo $output;
									?>
								<?php endif ?>
							</div>

						</div>

						<div class="et_pb_column et_pb_column_2_3 et_pb_column_3">
							<?php the_content(); ?>
						</div>
					</div>

					<div class="et_pb_row et_pb_row_3">

						<div class="et_pb_column et_pb_column_1_2 et_pb_column_4">
							<h2>Property Features</h2>
							<?php if (!empty($property_features)): ?>
								<?php
								// echo $property_floor_plan_text;
								$output = '<ul class="fa-ul">';
								foreach ($property_floor_plan_text_list as $floor_plan_li) {
									$output .= '<li><i class="fa-li fa fa-check"></i>' . $floor_plan_li . '</li>';
								}
								$output .= "</ul>";
								echo $output;
								?>
							<?php endif ?>
						</div>

						<div class="et_pb_column et_pb_column_1_2 et_pb_column_5">
							<h2>Apartment Features</h2>
							<?php if (!empty($apartment_features)): ?>
								<?php
								// echo $property_floor_plan_text;
								$output = '<ul class="fa-ul">';
								foreach ($apartment_floor_plan_text_list as $floor_plan_li) {
									$output .= '<li><i class="fa-li fa fa-check"></i>' . $floor_plan_li . '</li>';
								}
								$output .= "</ul>";
								echo $output;
								?>
							<?php endif ?>
						</div>
					</div>

					<div class="et_pb_row et_pb_row_4">

						<div class="et_pb_column et_pb_column_1_3 et_pb_column_6 information-col">
							<div class="contact-details">
								<div class="phone"><i class="fa fa-phone"></i><span><a href="tel:08162676716">08162676716</a></span></div>
								<div class="mobile"><i class="fa fa-mobile"></i><span><a href="tel:+2348162676716">+234 8162676716</a></span></div>
								<div class="email"><i class="fa fa-envelope-o"></i><span><a href="mailto:Mojola.l@ndr-services.com">Mojola.l@ndr-services.com</a></span></div>
								<div class="skype"><i class="fa fa-skype"></i><span>##############</</span></div>
							</div>
							<div class="social-media-icons">
								<ul>
									<li>
										<a href="<?php echo esc_url( et_get_option( 'divi_facebook_url', '#' ) ); ?>"><span class="fa-stack fa-lg">
											  <i class="fa fa-circle fa-stack-2x"></i>
											  <i class="fa fa-facebook fa-stack-1x"></i>
											</span></a>
									</li>
									<li>
										<a href="<?php echo esc_url( et_get_option( 'divi_twitter_url', '#' ) ); ?>"><span class="fa-stack fa-lg">
											  <i class="fa fa-circle fa-stack-2x"></i>
											  <i class="fa fa-twitter fa-stack-1x"></i>
											</span></a>
									</li>
									<li>
										<a href="<?php echo esc_url( et_get_option( 'divi_google_url', '#' ) ); ?>"><span class="fa-stack fa-lg">
											  <i class="fa fa-circle fa-stack-2x"></i>
											  <i class="fa fa-google-plus fa-stack-1x"></i>
											</span></a>
									</li>
									<li>
										<a href="https://uk.linkedin.com/in/stephen-williams-92a28b51">
											<span class="fa-stack fa-lg">
											  <i class="fa fa-circle fa-stack-2x"></i>
											  <i class="fa fa-linkedin fa-stack-1x"></i>
											</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="contact-name">LANRE MOJOLA</div>
						</div>

						<div class="et_pb_column et_pb_column_2_3 et_pb_column_7">
							<?php echo do_shortcode('[contact-form-7 id="232" title="Properties Page Form"]'); ?>
						</div>

					</div>

					<div class="et_pb_row et_pb_row_5">

						<div class="et_pb_column et_pb_column_1_2 et_pb_column_8">
							<a href="<?php echo home_url('properties') ?>" class="et_pb_button left">BACK TO PROPERTIES</a>
						</div>

					</div>

				</div>
			</div> <!-- .entry-content -->

		</article> <!-- .et_pb_post -->

	<?php endwhile; ?>

</div> <!-- #main-content -->

<script type="text/javascript">
 // document.getElementById('property-name').value = "<?php echo $slug ?>";
 // $('#property-name').val('<?php echo $slug ?>');
</script>


<?php get_footer(); ?>