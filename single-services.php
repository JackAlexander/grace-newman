<?php get_header(); ?>

<div id="main-content">

	<?php while ( have_posts() ) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php
			$slug = get_post_field( 'post_name', get_post() );
			$property_thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID));

			$service_features_title = get_post_meta( get_the_ID(), 'services-feature-title', true );

			$images = get_post_meta( get_the_ID(), 'meta-service-gallery-id', true );

			$service_features = get_post_meta( get_the_ID(), 'meta-service-features', true );
			$service_features_list = explode("\n", $service_features);

		?>
			<div class="entry-content">

			<?php
			switch ($wp_query->query_vars['custom_post_type_sub_page']) {
				case 'gallery':
					get_template_part('parts/service', 'gallery');
					break;
				default:
					get_template_part('parts/service', 'single');
					break;
			}
			?>

			</div> <!-- .entry-content -->

		</article> <!-- .et_pb_post -->

	<?php endwhile; ?>

</div> <!-- #main-content -->

<script type="text/javascript">
 // document.getElementById('property-name').value = "<?php echo $slug ?>";
 // $('#property-name').val('<?php echo $slug ?>');
</script>


<?php get_footer(); ?>