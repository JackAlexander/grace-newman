<?php 
	$images = get_post_meta( get_the_ID(), 'meta-service-gallery-id', true );
?>

<div class="et_pb_section et_pb_section_1 et_section_regular service-gallery-section-main">

	<div class="et_pb_row et_pb_row_1">
		<div class="et_pb_column et_pb_column_1_1 et_pb_column_0">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
			
	<div class="et_pb_row et_pb_row_2">
		<div class="et_pb_column et_pb_column_1_1 et_pb_column_0">

			<div class="prop-info">

				 <!-- masterslider -->
				 <div class="master-slider ms-skin-default" id="services-gallery">
				     <!-- new slide -->

    				 <?php foreach ($images as $image): ?>
	    				 	<div class="ms-slide">
	    				 		<?php echo wp_get_attachment_image($image['id'],'large') ?>

	    				 		<div class="ms-thumb"><?php echo wp_get_attachment_image($image['id'],'small') ?></div>
	    				 		<?php if (!empty($image['caption'])):?>
	    				 			<div class="gallery-caption-div"><?php echo $image['caption']; ?></div>
	    				 		<?php endif ?>
	    				 	</div>
    				 <?php endforeach; ?>

    				<!--  <div class="ms-thumb">lorem ipsum</div> -->
				  
				 </div>
				 <!-- end of masterslider -->
				
			</div>

		</div>
	</div>

</div>