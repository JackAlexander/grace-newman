<?php
	$slug = get_post_field( 'post_name', get_post() );
	$property_thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID));

	$service_features_title = get_post_meta( get_the_ID(), 'services-feature-title', true );

	$images = get_post_meta( get_the_ID(), 'meta-service-gallery-id', true );

	$service_features = get_post_meta( get_the_ID(), 'meta-service-features', true );
	$service_features_list = explode("\n", $service_features);

?>
<div class="et_pb_section et_pb_section_1 et_section_regular service-section-main">
	<div class="services-banner et_pb_gutters1">
		<div class="et_pb_row et_pb_row_2 ">
			<div class="et_pb_column et_pb_column_1_3 et_pb_column_0">
				<?php the_post_thumbnail( 'full' ); ?>
			</div>
			<div class="et_pb_column et_pb_column_2_3 et_pb_column_1">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
			
	<div class="et_pb_row et_pb_row_2">
		<div class="et_pb_column et_pb_column_1_1 et_pb_column_0">

			<div class="prop-info">
				
				
				<div class="service-info">
					<?php the_content(); ?>
				</div>
				
				<div class="service-features">
					<?php if (!empty($service_features)): ?>
						<h3 class="feature-title"><?php echo $service_features_title; ?></h3>
						<?php
						// echo $property_floor_plan_text;
						foreach ($service_features_list as $service_feature) {
							$output .= '<h3>' . '- &nbsp;' . $service_feature . '</h3>';
						}
						echo $output;
						?>
					<?php endif ?>
				</div>
			</div>

		</div>
	</div>

</div>