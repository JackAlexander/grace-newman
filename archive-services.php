<?php get_header(); ?>

<div id="main-content" class="properties-archive-content et_pb_gutters1">
	<div class="entry-content">
	<div id="content-area" class="clearfix">
		

			<?php query_posts($query_string . '&showposts=9&orderby=title&order=DESC'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class( array('et_pb_post') ); ?>>

					<?php
					$thumb = '';

					// $width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );
					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 350 );
					// $height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 233 );

					$classtext = 'et_pb_post_main_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$property_type = get_post_meta( get_the_ID(), 'meta-type', true );
					$property_bedrooms = get_post_meta( get_the_ID(), 'facility-bedrooms', true );
					$property_people = get_post_meta( get_the_ID(), 'facility-people', true );
					$property_garages = get_post_meta( get_the_ID(), 'facility-garages', true );
					$property_bathrooms = get_post_meta( get_the_ID(), 'facility-bathrooms', true );
					$thumb = $thumbnail["thumb"];
					$metas = get_post_meta( get_the_ID() );
					?>
					
					
						<div class="et_pb_section et_pb_section_1 et_section_regular ">

							<div class="et_pb_row et_pb_row_1">

								<div class="et_pb_column et_pb_column_1_4 et_pb_column_0 et_always_center_on_mobile">
									<div class="property-thumbnail">
										<a href="<?php the_permalink(); ?>">
											<?php print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height ); ?>
										</a>
										<div class="property-type">
											<span><?php echo $property_type ?></span>
										</div>
									</div>
									
								</div>

								<div class="et_pb_column et_pb_column_1_2 et_pb_column_1">
									<div class="property-title">
										<?php the_title(); ?>
									</div>
									<div class="property-summary">
										<?php the_excerpt(); ?>
									</div>
									<ul class="facilities">
										<li>
											<i class="fa fa-car"></i>
											<?php echo $property_garages; ?>
										</li>
										<li>
											<i class="fa fa-bed"></i>
											<?php echo $property_bedrooms; ?>
										</li>
										<li>
											<img src="http://ndr-services.com/wp-content/uploads/2016/04/Layer-6.png" alt="Bathrooms"><?php echo $property_bathrooms; ?>
										</li>
									</ul>
								</div>

								<div class="et_pb_column et_pb_column_1_4 et_pb_column_5">
									<a href="<?php the_permalink(); ?>" class="prop-more-button">MORE DETAILS</a>
								</div>

							</div>

						</div>
					</div>

				</article> <!-- .et_pb_post -->
				<?php
				endwhile;

				if ( function_exists( 'wp_pagenavi' ) )
					wp_pagenavi();
				else
					get_template_part( 'includes/navigation', 'index' );
				?>
			<?php else :
			get_template_part( 'includes/no-results', 'index' );
			endif; ?>

	</div>
</div> <!-- #main-content -->

<?php get_footer(); ?>