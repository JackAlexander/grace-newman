<?php

// Useful functions
function is_tree($pid){
	global $post;
	if (is_page()&&($post->post_parent==$pid||is_page($pid))) {
		return true;
	} else {
		return false;
	}
}

//Page Slug Body Class
function kjt_add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'kjt_add_slug_body_class' );

// Adds a custom select "list" to loop through the villas post type
function wpcf7_page_title( $tag ){
  
 if ( $tag['name'] == 'page-title' ) {
   $tag['values'][] = get_post_field( 'post_name', get_post() );
 }
 return $tag;

}
add_filter( 'wpcf7_form_tag', 'wpcf7_page_title' );

?>