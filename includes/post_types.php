<?php

if ( ! function_exists('kryps_post_types') ) {

	// Register Custom Post Type
	function kryps_post_types() {

		$labels = array(
			'name'                  => _x( 'Services', 'Post Type General Name', 'kjt-services' ),
			'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'kjt-services' ),
			'menu_name'             => __( 'Services', 'kjt-services' ),
			'name_admin_bar'        => __( 'Services', 'kjt-services' ),
			'archives'              => __( 'Service Archives', 'kjt-services' ),
			'parent_item_colon'     => __( 'Parent Service:', 'kjt-services' ),
			'all_items'             => __( 'All Services', 'kjt-services' ),
			'add_new_item'          => __( 'Add New Service', 'kjt-services' ),
			'add_new'               => __( 'Add New', 'kjt-services' ),
			'new_item'              => __( 'New Service', 'kjt-services' ),
			'edit_item'             => __( 'Edit Service', 'kjt-services' ),
			'update_item'           => __( 'Update Service', 'kjt-services' ),
			'view_item'             => __( 'View Service', 'kjt-services' ),
			'search_items'          => __( 'Search Service', 'kjt-services' ),
			'not_found'             => __( 'Not found', 'kjt-services' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'kjt-services' ),
			'featured_image'        => __( 'Featured Image', 'kjt-services' ),
			'set_featured_image'    => __( 'Set featured image', 'kjt-services' ),
			'remove_featured_image' => __( 'Remove featured image', 'kjt-services' ),
			'use_featured_image'    => __( 'Use as featured image', 'kjt-services' ),
			'insert_into_item'      => __( 'Insert into Service', 'kjt-services' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Service', 'kjt-services' ),
			'items_list'            => __( 'Services list', 'kjt-services' ),
			'items_list_navigation' => __( 'Services list navigation', 'kjt-services' ),
			'filter_items_list'     => __( 'Filter Services list', 'kjt-services' ),
		);
		$args = array(
			'label'                 => __( 'Service', 'kjt-services' ),
			'description'           => __( 'Add, modify and remove Services to your website in an organised manner.', 'kjt-services' ),
			'labels'                => $labels,
			// 'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', ),
			'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'page-attributes', ),
			// 'taxonomies'            => array( 'category', 'post_tag' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-admin-multisite',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,		
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
		);
		register_post_type( 'Services', $args );
	}
add_action( 'init', 'kryps_post_types', 0 );

include 'meta_boxes.php';

}

// Adds Divi Builder to Villa post type
/* Enable Divi Builder on all post types with an editor box */
function myprefix_add_post_types($post_types) {
	foreach(get_post_types() as $pt) {
		if (!in_array($pt, $post_types) and post_type_supports($pt, 'editor')) {
			$post_types[] = $pt;
		}
	} 
	return $post_types;
}
// add_filter('et_builder_post_types', 'myprefix_add_post_types');

/* Add Divi Custom Post Settings box */
function myprefix_add_meta_boxes() {
	foreach(get_post_types() as $pt) {
		if (post_type_supports($pt, 'editor') and function_exists('et_single_settings_meta_box')) {
			add_meta_box('et_settings_meta_box', __('Service Settings', 'Divi'), 'et_single_settings_meta_box', $pt, 'side', 'high');
		}
	} 
}
// add_action('add_meta_boxes', 'myprefix_add_meta_boxes');

// Adds a custom select "list" to loop through the villas post type
function wpcf7_villa_list ( $tag, $unused ){
  $tag['raw_values'][] = "";
  $tag['values'][] = "";
  $tag['labels'][] = "Choose a Service";

  if ( $tag['name'] != 'villa-list' )
    return $tag;

  $args = array(
  	'numberposts' => -1,
  	'post_type' => 'villas',
    'orderby' => 'title',
    'order' => 'ASC'
  );

  $villas = get_posts($args);

  if ( !$villas )
    return $tag;

  foreach ( $villas as $villa ){
    $tag['raw_values'][] = $villa->post_title;
    $tag['values'][] = $villa->post_title;
    $tag['labels'][] = $villa->post_title;
  }
  return $tag;
}
// add_filter( 'wpcf7_form_tag', 'wpcf7_villa_list', 10, 2);

?>