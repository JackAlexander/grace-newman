<?php
function kjt_meta_box_enqueues($hook) {
	global $post_type;
	if ( 'post.php' == $hook || 'post-new.php' == $hook ) {
		if ( 'services' == $post_type ){
			wp_enqueue_script('gallery-metabox', get_stylesheet_directory_uri() . '/assets/js/meta-box-image.js', array('jquery', 'jquery-ui-sortable'));
			wp_enqueue_style('gallery-metabox', get_stylesheet_directory_uri() . '/assets/css/admin.css');
		}
	}
}
add_action('admin_enqueue_scripts', 'kjt_meta_box_enqueues');
function kjt_meta_boxes(){
	add_meta_box( 'kjt_services_main_meta', __( 'Service Lists', 'kjt-services' ), 'services_main_meta_callback', 'services', 'normal', 'high' );
	add_meta_box( 'kjt_services_gallery_meta', __( 'Service Gallery', 'kjt' ), 'kjt_services_gallery_meta_cb', 'services', 'normal', 'high' );
}
add_action( 'add_meta_boxes', 'kjt_meta_boxes' );

/**
 * Outputs the content of the main meta box
 */

function services_main_meta_callback( $post ) {
	wp_nonce_field( basename( __FILE__ ), 'services_main_nonce' );
	$services_stored_meta = get_post_meta( $post->ID );
	?>

	<div class="services-row">
		<div class="services-row-content">
			<label for="services-feature-title"><?php _e( 'Features header', 'kjt-services' )?></label>
			<input type="text" name="services-feature-title" id="services-feature-title" value="<?php if ( isset ( $services_stored_meta['services-feature-title'] ) ) echo $services_stored_meta['services-feature-title'][0]; ?>" />

			<label for="meta-service-features" class="services-row-title"><?php _e( 'Service Features', 'kjt-services' )?></label>
			<textarea name="meta-service-features" id="meta-service-features" style="width:100%;"><?php if ( isset ( $services_stored_meta['meta-service-features'] ) ) echo esc_textarea($services_stored_meta['meta-service-features'][0]); ?></textarea>
		</div>
	</div>

	<?php
}

/**
* Outputs the content of the team side meta box
*/
function kjt_services_gallery_meta_cb( $post ){
	wp_nonce_field( basename( __FILE__ ), 'kjt_service_main_nonce' );
	$service_stored_meta = get_post_meta( $post->ID );
	$gallery_ids = get_post_meta( $post->ID, 'meta-service-gallery-id', true );
	$service_skills = get_post_meta( $post->ID, 'meta-service-skills', true );
?>

	<div class="portfolio-row">
		<div id="gallery-metabox" class="portfolio-col">
			<label for="meta-service-gallery-id" class="gallery-meta-title"><?php _e( 'Service Gallery', 'kjt' ); ?></label>
			<button class="gallery-add button" type="button" data-uploader-title="Add image(s) to service" data-uploader-button-text="Add image(s)">Add image(s)</button>
			<ul id="gallery-metabox-list">
			<?php if ($gallery_ids) : ?>
				<?php foreach ($gallery_ids as $gallery_img => $data) : $image = wp_get_attachment_image_src($data['id']); ?>
			  <li>
			    <input type="hidden" name="meta-service-gallery-id[<?php echo $gallery_img; ?>][id]" value="<?php echo $data['id']; ?>">
			    <img class="image-preview" src="<?php echo $image[0]; ?>">
			    <textarea name="meta-service-gallery-id[<?php echo $gallery_img; ?>][caption]"><?php echo $data['caption']; ?></textarea>
			    <button class="change-image button button-small" type="button" data-uploader-title="Change image" data-uploader-button-text="Change image">Change image</button><br>
			    <small><button class="remove-image button button-small" type="button">Remove image</button></small>
			  </li>
				<?php endforeach; ?>
			<?php endif; ?>
			</ul>
		</div>
	</div>

<?php
}

/**
 * Saves the custom team meta input
 */
function kjt_service_meta_save( $post_id ) {
 
	// Checks save status
	$is_autosave = wp_is_post_autosave( $post_id );
	$is_revision = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST[ 'kjt_service_main_nonce' ] ) && wp_verify_nonce( $_POST[ 'kjt_service_main_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

	// Exits script depending on save status
	if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
		return;
	}
 
	// Checks for input and sanitizes/saves if needed
	if( isset( $_POST[ 'meta-service-gallery-id' ] ) ) {
		update_post_meta( $post_id, 'meta-service-gallery-id', $_POST[ 'meta-service-gallery-id' ] );
	} else {
		delete_post_meta( $post_id, 'meta-service-gallery-id' );
	}

	// Checks for input and sanitizes/saves if needed
	if( isset( $_POST[ 'meta-service-features' ] ) ) {
		update_post_meta( $post_id, 'meta-service-features', strtoupper($_POST[ 'meta-service-features' ]) );
	} else {
		delete_post_meta( $post_id, 'meta-service-features' );
	}

	// Checks for input and sanitizes/saves if needed
	if( isset( $_POST[ 'services-feature-title' ] ) ) {
		update_post_meta( $post_id, 'services-feature-title', strtoupper($_POST[ 'services-feature-title' ]) );
	} else {
		delete_post_meta( $post_id, 'services-feature-title' );
	}

}
add_action( 'save_post', 'kjt_service_meta_save' );

?>