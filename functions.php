<?php

function kryps_enqueue_scripts() {
  wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
	wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');

	wp_enqueue_script( 'main-js', get_stylesheet_directory_uri() . '/assets/js/allaboutgrace-0.1.0.min.js', array( 'jquery' ),null,true );

	if ( is_singular( 'services' )) {
		$plugins_path = plugins_url();

		wp_enqueue_style( 'kjt-masterslider-style', $plugins_path . '/master-slider/public/assets/css/masterslider.main.min.css' );
		wp_enqueue_style( 'kjt-masterslider-default-style', $plugins_path . '/master-slider/public/assets/css/ms-skin-sample.css' );
		wp_enqueue_script( 'kjt-masterslider-core', $plugins_path . '/master-slider/public/assets/js/masterslider.min.js', array( 'jquery', 'jquery-easing' ), '1.0.0', true );
	}
}
add_action( 'wp_enqueue_scripts', 'kryps_enqueue_scripts' );

function pp_setup_theme() {

	include get_stylesheet_directory() . '/includes/post_types.php';

	// include( $template_directory . '/includes/widgets.php' );

}
add_action( 'after_setup_theme', 'pp_setup_theme' );

function custom_post_type_sub_page_urls(){
  add_rewrite_tag('%custom_post_type_sub_page%', '([^&]+)');
  add_rewrite_rule(
    'services/([^/]*)/([^/]*)',
    'index.php?services=$matches[1]&custom_post_type_sub_page=$matches[2]',
    'top'
  );
  flush_rewrite_rules();
}
add_action('init', 'custom_post_type_sub_page_urls');

require_once get_stylesheet_directory() . '/includes/core.php';


/* Disable WordPress Admin Bar for all users but admins. */
// show_admin_bar(false);

?>